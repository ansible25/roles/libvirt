Libvirt
=========

Role that installs Libvirt

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Installing Libvirt on targeted machine:

    - hosts: servers
      roles:
         - libvirt

License
-------

[MIT](LICENSE)

Author Information
------------------

This role was created by [bradthebuilder](https://bradthebuilder.me)
